package veureka.pt.services;

import android.content.Context;

import org.apache.log4j.Logger;

import veureka.pt.services.factory.LoggerFactory;
import veureka.pt.services.util.constant.CommonsConstant;

public class ExtLogger extends Logger {
    
    private static LoggerFactory loggerFactory = new LoggerFactory();
    public static String username = CommonsConstant.DEFAULT_USERNAME;
    
    public ExtLogger(String name) {
        super(name);
    }
    
    public static ExtLogger getLogger(Class loggerClass) {
        return (ExtLogger) Logger.getLogger(loggerClass.getName(), loggerFactory);
    }
    
    public void error(Context context, Throwable t, String errorId, Object... messageArguments) {
        String messageError = BundleManager.getInstance().displayMessage(context, errorId, messageArguments);
        super.error(messageError, t);
    }
    
    @SuppressWarnings("unused")
    public void fatal(Context context, Throwable t, String errorId, Object... messageArguments) {
        String messageError = BundleManager.getInstance().displayMessage(context, errorId, messageArguments);
        super.fatal(messageError, t);
    }
}
