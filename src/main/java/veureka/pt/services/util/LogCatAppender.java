package veureka.pt.services.util;

import android.util.Log;

import org.apache.log4j.AppenderSkeleton;
import org.apache.log4j.Layout;
import org.apache.log4j.Level;
import org.apache.log4j.PatternLayout;
import org.apache.log4j.spi.LoggingEvent;

import veureka.pt.services.ExtLogger;
import veureka.pt.services.util.constant.CommonsConstant;

public class LogCatAppender extends AppenderSkeleton {
    
    private Layout tagLayout;
    
    public LogCatAppender(final Layout messageLayout) {
        this.tagLayout = new PatternLayout("%c");
        setLayout(messageLayout);
    }
    
    @Override
    protected void append(final LoggingEvent le) {
        
        if(ExtLogger.username == null) {
            ExtLogger.username = CommonsConstant.DEFAULT_USERNAME;
        }
        le.setProperty("username", ExtLogger.username);
        
        switch(le.getLevel().toInt()) {
            case Level.TRACE_INT:
                if(le.getThrowableInformation() != null) {
                    Log.v(getTagLayout().format(le), getLayout().format(le),
                          le.getThrowableInformation().getThrowable());
                } else {
                    Log.v(getTagLayout().format(le), getLayout().format(le));
                }
                break;
            case Level.DEBUG_INT:
                if(le.getThrowableInformation() != null) {
                    Log.d(getTagLayout().format(le), getLayout().format(le),
                          le.getThrowableInformation().getThrowable());
                } else {
                    Log.d(getTagLayout().format(le), getLayout().format(le));
                }
                break;
            case Level.INFO_INT:
                if(le.getThrowableInformation() != null) {
                    Log.i(getTagLayout().format(le), getLayout().format(le),
                          le.getThrowableInformation().getThrowable());
                } else {
                    Log.i(getTagLayout().format(le), getLayout().format(le));
                }
                break;
            case Level.WARN_INT:
                if(le.getThrowableInformation() != null) {
                    Log.w(getTagLayout().format(le), getLayout().format(le),
                          le.getThrowableInformation().getThrowable());
                } else {
                    Log.w(getTagLayout().format(le), getLayout().format(le));
                }
                break;
            case Level.ERROR_INT:
                if(le.getThrowableInformation() != null) {
                    Log.e(getTagLayout().format(le), getLayout().format(le),
                          le.getThrowableInformation().getThrowable());
                } else {
                    Log.e(getTagLayout().format(le), getLayout().format(le));
                }
                break;
            case Level.FATAL_INT:
                if(le.getThrowableInformation() != null) {
                    Log.wtf(getTagLayout().format(le), getLayout().format(le),
                            le.getThrowableInformation().getThrowable());
                } else {
                    Log.wtf(getTagLayout().format(le), getLayout().format(le));
                }
                break;
        }
    }
    
    @Override
    public void close() {
    }
    
    @Override
    public boolean requiresLayout() {
        return true;
    }
    
    private Layout getTagLayout() {
        return tagLayout;
    }
}
