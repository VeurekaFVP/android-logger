package veureka.pt.services.util;

import com.google.firebase.crash.FirebaseCrash;

import org.apache.log4j.AppenderSkeleton;
import org.apache.log4j.Layout;
import org.apache.log4j.spi.LoggingEvent;

import veureka.pt.services.ExtLogger;
import veureka.pt.services.util.constant.CommonsConstant;

public class FirebaseCrashAppender extends AppenderSkeleton {
    
    public FirebaseCrashAppender(final Layout messageLayout) {
        setLayout(messageLayout);
    }
    
    @Override
    protected void append(final LoggingEvent le) {
        
        if(ExtLogger.username == null) {
            ExtLogger.username = CommonsConstant.DEFAULT_USERNAME;
        }
        le.setProperty("username", ExtLogger.username);
        
        FirebaseCrash.log(getLayout().format(le));
        if(le.getThrowableInformation() != null) {
            FirebaseCrash.report(le.getThrowableInformation().getThrowable());
        }
    }
    
    @Override
    public void close() {
    }
    
    @Override
    public boolean requiresLayout() {
        return true;
    }
}
