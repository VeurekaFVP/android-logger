package veureka.pt.services.util.constant;

public interface CommonsConstant {
    
    String DEFAULT_USERNAME = "undefined";
    String DEFAULT_LOG_PATTERN = "[%-5p] %c | %X{username} | %m%n";
}
