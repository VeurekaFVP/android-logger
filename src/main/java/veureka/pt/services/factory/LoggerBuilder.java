package veureka.pt.services.factory;

import org.apache.log4j.AsyncAppender;
import org.apache.log4j.Layout;
import org.apache.log4j.Level;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.log4j.PatternLayout;
import org.apache.log4j.helpers.LogLog;

import veureka.pt.services.util.FirebaseCrashAppender;
import veureka.pt.services.util.LogCatAppender;
import veureka.pt.services.util.constant.CommonsConstant;

public class LoggerBuilder {
    
    private Level rootLevel = Level.DEBUG;
    private String logPattern = CommonsConstant.DEFAULT_LOG_PATTERN;
    
    private boolean useLogCatAppender = true;
    private boolean useFirebaseCrashAppender = true;
    private boolean resetConfiguration = true;
    private boolean internalDebugging = false;
    
    public LoggerBuilder() {}
    
    public LoggerBuilder rootLevel(final Level level) {
        this.rootLevel = level;
        return this;
    }
    
    public LoggerBuilder logPattern(final String logPattern) {
        this.logPattern = logPattern;
        return this;
    }
    
    @SuppressWarnings("unused")
    public LoggerBuilder addLevel(final String loggerName, final Level level) {
        Logger.getLogger(loggerName).setLevel(level);
        return this;
    }
    
    public LoggerBuilder useFirebaseCrashAppender(final boolean useFirebaseCrashAppender) {
        this.useFirebaseCrashAppender = useFirebaseCrashAppender;
        return this;
    }
    
    public LoggerBuilder useLogCatAppender(final boolean useLogCatAppender) {
        this.useLogCatAppender = useLogCatAppender;
        return this;
    }
    
    @SuppressWarnings("unused")
    public LoggerBuilder resetConfiguration(boolean resetConfiguration) {
        this.resetConfiguration = resetConfiguration;
        return this;
    }
    
    public LoggerBuilder internalDebugging(boolean internalDebugging) {
        this.internalDebugging = internalDebugging;
        return this;
    }
    
    public void builder() {
        final Logger root = Logger.getRootLogger();
        
        if(resetConfiguration) {
            LogManager.getLoggerRepository().resetConfiguration();
        }
        
        LogLog.setInternalDebugging(internalDebugging);
        
        if(useFirebaseCrashAppender) {
            configureFirebaseCrashAppender();
        }
        
        if(useLogCatAppender) {
            configureLogCatAppender();
        }
        
        root.setLevel(rootLevel);
    }
    
    private void configureFirebaseCrashAppender() {
        final Logger root = Logger.getRootLogger();
        final AsyncAppender asyncAppender = new AsyncAppender();
        final FirebaseCrashAppender firebaseCrashAppender;
        final Layout firebaseLayout = new PatternLayout(logPattern);
        
        firebaseCrashAppender = new FirebaseCrashAppender(firebaseLayout);
        asyncAppender.setName("asyncFirebaseCrashAppender");
        asyncAppender.addAppender(firebaseCrashAppender);
        
        root.addAppender(asyncAppender);
    }
    
    private void configureLogCatAppender() {
        final Logger root = Logger.getRootLogger();
        final Layout logCatLayout = new PatternLayout(logPattern);
        final LogCatAppender logCatAppender = new LogCatAppender(logCatLayout);
        
        root.addAppender(logCatAppender);
    }
}
