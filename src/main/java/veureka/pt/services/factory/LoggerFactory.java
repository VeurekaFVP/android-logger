package veureka.pt.services.factory;

import org.apache.log4j.Logger;

import veureka.pt.services.ExtLogger;

public class LoggerFactory implements org.apache.log4j.spi.LoggerFactory {
    
    public LoggerFactory() {
    }
    
    @Override
    public Logger makeNewLoggerInstance(String name) {
        return new ExtLogger(name);
    }
}
